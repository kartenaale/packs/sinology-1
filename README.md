# Sinologie STEOP
Anki Decks für verschiedene Lehrveranstaltungen, die viele Studierende im 1.
Semester belegen und die für die STEOP-Prüfung relevant sind.

Sie helfen dir beim Chinesisch lernen und bei STEOP-relevanten Vorlesungen zur
Prüfungsvorbereitung.

## Download
Die neueste Version der APKG-Dateien gibt es unter
[Releases](https://gitlab.phaidra.org/kartenaale/packs/sinology-1/-/releases).

Importiere sie in Anki um loszulegen. Wenn dir Anki noch neu ist, kannst du
auch die _Anleitung für neue Benutzer:innen_ auf
[Englisch](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/GUIDE.md)
oder
[Deutsch](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/ANLEITUNG.md)
zurateziehen.

## Wie sehen die Karten aus?
Eine Karte zum Schreiben üben sieht so aus:

<img src="https://gitlab.phaidra.org/kartenaale/card-templates/-/raw/main/screenshots/example_molaoshi.png" alt="Writing" width="400">

Karten für Faktenwissen so:

<img src="https://gitlab.phaidra.org/kartenaale/card-templates/-/raw/main/screenshots/example_facts.png" alt="Q/A" width="400">

## Was ist enthalten?
* `Chinesisch-lernen-fuer-die-Sinologie,-Band-1-VERSION.apkg`: Der erste Band von _Chinesisch lernen für die Sinologie_ von Xia Baige und Wolfgang Zeidl: Vokabelkarten, Strichtypen, Pianpang
* `Einführung-in-die-politische-Geschichte-Chinas-2.0.0.apkg`: Theorie
* `Introduction-to-Chinese-Cultural-History-3.0.0.apkg`: Theorie

## Dieses Pack verbessern
Wenn du Fehler in diesem Pack oder in anderen findest, oder wenn du eigene
Anki-Inhalte hast die du teilen möchtest, melde dich auf
[Whatsapp](https://chat.whatsapp.com/JFKpfmq29yM2xKcSu7JQib).
Wenn du einen Gitlab-Account hast, kannst du auch ein
[Issue anlegen](https://gitlab.phaidra.org/kartenaale/packs/sinology-1/-/issues/new).

